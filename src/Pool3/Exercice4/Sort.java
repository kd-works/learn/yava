package Pool3.Exercice4;

import java.util.ArrayList;
import java.util.Random;

public class Sort {
    public static void main(String[] args) {
        int n = 100000;

        ArrayList<Integer> t = new ArrayList<>(n);

        Random r = new Random();

        for (int k = 0; k < n; k++) {
            t.add(r.nextInt());
        }

        long before = System.currentTimeMillis();

        for (int i = 0; i < n - 2; i++) {
            for (int j = i + 1; j < n - 1; j++) {
                if (t.get(i) > t.get(j)) {
                    int temp = t.get(i);
                    t.set(i, t.get(j));
                    t.set(j, temp);
                }
            }
        }

        long after = System.currentTimeMillis();
        System.out.println("Time : " + (after - before) + "ms");
    }
}
