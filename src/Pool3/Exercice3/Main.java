package Pool3.Exercice3;

import Pool3.Exercice1.ListArray;
import Pool3.Exercice1.ListLinked;

public class Main {
    public static void main(String[] args) {
        ListArray listArray = new ListArray(30000);
        ListLinked listLinked = new ListLinked(30000);

        listArray.get();
        listLinked.get();

        System.out.println(listArray.toString());
        System.out.println(listLinked.toString());
    }
}
