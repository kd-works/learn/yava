package Pool3.Exercice1;

public class Main {
    public static void main(String[] args) {
        ListArray listArray = new ListArray(30000);
        ListLinked listLinked = new ListLinked(30000);

        listArray.addFirst();
        listLinked.addFirst();

        System.out.println(listArray.toString());
        System.out.println(listLinked.toString());
    }
}
