package Pool3.Exercice1;

import java.util.LinkedList;
import java.util.Random;

public class ListLinked {
    private LinkedList<Integer> list;
    private long time;
    private int delay;

    public ListLinked(int delay) {
        this.list = new LinkedList<>();
        this.delay = delay;
    }

    void addFirst() {
        long before = System.currentTimeMillis();

        for (int i = 0; i < this.delay; i++) {
            this.list.addFirst(i);
        }

        long after = System.currentTimeMillis();
        this.time = after - before;
    }

    public void addLast() {
        long before = System.currentTimeMillis();

        for (int i = 0; i < this.delay; i++) {
            this.list.addLast(i);
        }

        long after = System.currentTimeMillis();
        this.time = after - before;
    }

    public void addMiddle() {
        long before = System.currentTimeMillis();

        for (int i = 0; i < this.delay; i++) {
            this.list.add(i);
        }

        long after = System.currentTimeMillis();
        this.time = after - before;
    }

    public void get() {
        long before = System.currentTimeMillis();
        Random r = new Random();

        for (int i = 0; i < this.delay; i++) {
            this.list.get(r.nextInt(i));
        }

        long after = System.currentTimeMillis();
        this.time = after - before;
    }

    public String toString() {
        return "LinkedList : " + this.time + "ms";
    }
}
