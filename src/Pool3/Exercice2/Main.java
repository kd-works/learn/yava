package Pool3.Exercice2;

import Pool3.Exercice1.ListArray;
import Pool3.Exercice1.ListLinked;

public class Main {
    public static void main(String[] args) {
        last();
        middle();
    }

    private static void last() {
        ListArray listArray = new ListArray(30000);
        ListLinked listLinked = new ListLinked(30000);

        listArray.addLast();
        listLinked.addLast();

        System.out.println(listArray.toString());
        System.out.println(listLinked.toString());
    }

    private static void middle() {
        ListArray listArray = new ListArray(30000);
        ListLinked listLinked = new ListLinked(30000);

        listArray.addMiddle();
        listLinked.addMiddle();

        System.out.println(listArray.toString());
        System.out.println(listLinked.toString());
    }
}
