package Pool3.Exercice5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Coolection {
    public static void main(String[] args) {
        int n = 100000;

        ArrayList<Integer> t = new ArrayList<>(n);

        Random r = new Random();

        for (int k = 0; k < n; k++) {
            t.add(r.nextInt());
        }

        long before = System.currentTimeMillis();
        Collections.sort(t);
        long after = System.currentTimeMillis();

        System.out.println("Time : " + (after - before) + "ms");
    }
}
