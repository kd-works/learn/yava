package Pool2.Exploration;

class Config {
    /**
     * @param month month number
     * @return how many days a month have
     */
    static int DaysInMonth(int month) {
        int[] months = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        return months[month - 1];
    }

    /**
     * @param year year number
     * @return how many days a year have
     */
    static int DaysInYear(int year) {
        return year % 400 == 0 ? 365 : 364;
    }

    /**
     * @return how many month a year have
     */
    static int MonthInYear() {
        return 12;
    }

    static String DayName(int day) {
        return String.valueOf(Days.values()[day % Days.values().length - 1]);
    }

    static String MonthName(int month) {
        return String.valueOf(Months.values()[month % Months.values().length - 1]);
    }

    enum Days {
        Lundi,
        Mardi,
        Mercredi,
        Jeudi,
        Vendredi,
        Samedi,
        Dimanche
    }

    enum Months {
        Janvier,
        Février,
        Mars,
        Avril,
        Mai,
        Juin,
        Juillet,
        Aout,
        Septembre,
        Octobre,
        Novembre,
        Décembre
    }
}
