package Pool2.Exploration;

public class Date extends Pool1.Exercice6.Date {
    private int day;
    private int month;
    private int year;

    // Lundi 1 Janvier 1970
    private int[] ref = new int[]{1, 1, 1, 1970};
    private String dayName;
    private String monthName;
    private int dayNameInt;

    public Date() {
        this.day = 1;
        this.month = 1;
        this.year = 1;
    }

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public Date(int day1, int day2, int month, int year) {
        this.dayNameInt = day1;
        this.day = day2;
        this.month = month;
        this.year = year;
    }

    void createDate() {
        Date ref = new Date(this.ref[0], this.ref[1], this.ref[2], this.ref[3]);

        System.out.println("Ref: " + ref.toString());
        System.out.println("Actual: " + this.toString());

        while (!ref.toString().equals(this.toString())) {
            ref.incr();
        }

        System.out.println("Ref (futur): " + ref);

        this.dayName = Config.DayName(ref.dayNameInt);
        this.monthName = Config.MonthName(ref.month);

        System.out.println("\n" + this.dayName + " " + this.day + " " + this.monthName + " " + this.year);
    }

    public void incr() {
        if (this.day >= Config.DaysInMonth(this.month)) {
            if (this.month >= 12) {
                this.day = 1;
                this.month = 1;
                this.year++;
            } else {
                this.day = 1;
                this.month++;
            }
        } else {
            this.day++;
            this.dayNameInt++;
        }
    }

    public String toString() {
        return this.day + "/" + this.month + "/" + this.year;
    }
}
