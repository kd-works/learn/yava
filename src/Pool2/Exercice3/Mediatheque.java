package Pool2.Exercice3;

import Pool2.Exercice1.Document;

import java.util.ArrayList;

public class Mediatheque {
    private ArrayList<Document> documents;

    public Mediatheque() {
        this.documents = new ArrayList<>();
    }

    public void addDocument(Document doc) {
        this.documents.add(doc);
    }

    private int nDocs() {
        return this.documents.size();
    }

    private Document getDoc(int i) {
        return this.documents.get(i);
    }

    public String getDocument(int id) {
        return "\n" + getDoc(id);
    }

    public void getDocumentByAuthor(String author) {
        System.out.println("-----------------------------------------------------\n\n");
        System.out.println("Voici la liste des documents avec " + author + " en tant qu'auteur.");

        for (Document doc : this.documents)
            if (doc.getDocAuthor().equals(author))
                System.out.println("\n" + doc);
    }

    public String toString() {
        return "Médiathèque : \n  Bienvenue dans la Médiathèque !\n  Nombre de documents enregistrés à ce jour : " + nDocs() + "\n\n-----------------------------------------------------";
    }
}
