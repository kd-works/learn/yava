package Pool2.Exercice1;

public class Video extends Document {
    private int duration;
    private String genre;

    Video(String name, String genre, String author, int duration) {
        super(name, author, "Video");

        this.duration = duration;
        this.genre = genre;
    }

    private int getDuration() {
        return this.duration;
    }

    private String getGenre() {
        return this.genre;
    }

    public String toString() {
        return super.toString() + "\nVideo : \n  Genre : " + getGenre() + "\n  Durée : " + getDuration();
    }
}
