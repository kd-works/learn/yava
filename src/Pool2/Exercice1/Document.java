package Pool2.Exercice1;

public class Document {
    private String docName;
    private String docAuthor;
    private String docType;

    public Document(String docName, String docAuthor, String docType) {
        this.docName = docName;
        this.docAuthor = docAuthor;
        this.docType = docType;
    }

    private String getDocName() {
        return this.docName;
    }

    private String getDocType() {
        return this.docType;
    }

    public String getDocAuthor() {
        return this.docAuthor;
    }

    public String toString() {
        return "Document : \n  Nom : " + getDocName() + "\n  Auteur : " + getDocAuthor() + "\n  Type : " + getDocType() + "\n";
    }
}
