package Pool2.Exercice5;

import java.util.InputMismatchException;

public class ErrorException extends Exception {
    private Exception error;
    private String name;
    private String oopName;

    ErrorException(RuntimeException error, String name, String oopName) {
        this.error = error;
        this.name = name;
        this.oopName = oopName;
    }

    private Exception getError() {
        return this.error;
    }

    private String getName() {
        return this.name;
    }

    private String getOopName() {
        return this.oopName;
    }

    public void input(InputMismatchException e) {
        this.error = e;
    }

    public String toString() {
        return "ERROR!\n  Error Code : " + getError() + "\n  Name Value : " + getName() + "\n  this.name Value : " + getOopName();
    }
}
