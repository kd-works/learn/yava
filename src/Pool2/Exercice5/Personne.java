package Pool2.Exercice5;

public class Personne {
    private String nom = "";

    Personne(String nom) throws ErrorException {
        try {
            System.out.println(nom);
        } catch (RuntimeException e) {
            throw new ErrorException(e, nom, this.nom);
        }
    }

    private String getNom() {
        return this.nom;
    }

    public String toString() {
        return "OK! " + getNom();
    }
}
