package Pool2.Exercice10;

import Pool1.Exercice6.Date;

public class JeSaitPasCommentTeNommer {
    public static void main(String[] args) {
        UneCertaineClasseStatiqueDApresLEnnonce();
    }

    /**
     * Car dans le tas la référence de d1 est différente de d2
     */
    private static void UneCertaineClasseStatiqueDApresLEnnonce() {
        Date d1 = new Date(1, 2, 2003);
        Date d2 = new Date(1, 2, 2003);

        if (d1.equals(d2)) {
            System.out.println("Les dates sont identiques");
        } else {
            System.out.println("Les dates sont différentes");
        }
    }
}
