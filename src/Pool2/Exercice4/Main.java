package Pool2.Exercice4;

import Pool2.Exercice2.Roman;
import Pool2.Exercice3.Mediatheque;

public class Main {
    public static void main(String[] args) {
        Mediatheque mediatheque = new Mediatheque();

        String[][] books = {
                {"Alice in Wonderland", "Poo", "Fantasy Book"},
                {"Peter Pan", "Poo", "Old Book"},
                {"Winnie the Poo", "Poop", "Child Book"},
                {"Le monde s'effondre", "Chinua Achebe", "Romanesque"}
        };

        for (String[] book : books)
            mediatheque.addDocument(new Roman(book[0], book[1], book[2]));

        System.out.println(mediatheque.toString());
        System.out.println(mediatheque.getDocument(2));
        mediatheque.getDocumentByAuthor("Chinua Achebe");
    }
}
