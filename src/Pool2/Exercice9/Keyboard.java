package Pool2.Exercice9;

import Pool2.Exercice8.ErrorException2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Keyboard extends Pool2.Exercice8.Keyboard {
    public int lireInt() throws ErrorException2 {
        try {
            return new Scanner(System.in).nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Lol nope!");
            return lireInt();
        }
    }
}
