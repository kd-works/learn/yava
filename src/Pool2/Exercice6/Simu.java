package Pool2.Exercice6;

public class Simu {
    protected static void stat(int nExp) throws DivZero {
        try {
            System.out.println(1 / nExp);
        } catch (ArithmeticException e) {
            throw new DivZero(e);
        }
    }
}
