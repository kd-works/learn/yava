package Pool2.Exercice6;

public class DivZero extends Exception {
    private Exception error;
    private ArithmeticException runtimeException;

    DivZero(ArithmeticException error) {
        this.runtimeException = new ArithmeticException();
        this.error = error;
    }

    private Exception getError() {
        return this.error;
    }

    public String toString() {
        return "ERROR!\n  Error Code : " + getError() + "\n  " + this.runtimeException;
    }
}
