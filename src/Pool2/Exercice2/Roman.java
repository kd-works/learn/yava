package Pool2.Exercice2;

public class Roman extends Livre {
    private String name;
    private String author;
    private String genre;

    public Roman(String name, String author, String genre) {
        super(name, author, "Roman");

        this.name = name;
        this.author = author;
        this.genre = genre;
    }

    private String getName() {
        return this.name;
    }

    private String getAuthor() {
        return this.author;
    }

    private String getGenre() {
        return this.genre;
    }

    public String toString() {
        return super.toString() + "\nRoman : \n  Genre : " + getGenre() + "\n";
    }
}
