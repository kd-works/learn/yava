package Pool2.Exercice2;

import Pool2.Exercice1.Document;

public class Livre extends Document {
    private String name;
    private String author;
    private String type;

    Livre(String name, String author, String type) {
        super(name, author, "Livre");

        this.name = name;
        this.author = author;
        this.type = type;
    }

    private String getName() {
        return this.name;
    }

    private String getAuthor() {
        return this.author;
    }

    private String getType() {
        return this.type;
    }

    public String toString() {
        return super.toString() + "\nLivre : \n  Type : " + getType() + "\n";
    }
}
