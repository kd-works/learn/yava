package Pool2.Exercice8;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Keyboard {
    public Keyboard() {
        //
    }

    public int lireInt() throws ErrorException2 {
        try {
            return new Scanner(System.in).nextInt();
        } catch (InputMismatchException e) {
            return Integer.parseInt(null);
        }
    }
}
