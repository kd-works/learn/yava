package Pool2.Exercice8;

import java.util.InputMismatchException;

public class ErrorException2 extends Exception {
    private Exception error;

    public ErrorException2(InputMismatchException error) {
        this.error = error;
    }

    public String toString() {
        return "ERROR!\n  Error Code : " + this.error;
    }
}
