package Pool1.Exercice5;

public class Date extends Pool1.Exercice4.Date {
    public int day;
    public int month;
    public int year;

    public Date() {
       this.day = 1;
       this.month = 1;
       this.year = 1;
    }

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public void incr() {
        if (this.day >= nbJours(this.month)) {
            if (this.month >= 12) {
                this.day = 1;
                this.month = 1;
                this.year++;
            } else {
                this.day = 1;
                this.month++;
            }
        } else {
            this.day++;
        }
    }

    public String toString() {
        return this.day + "/" + this.month + "/" + this.year;
    }
}
