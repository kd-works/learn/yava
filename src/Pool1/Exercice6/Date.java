package Pool1.Exercice6;

public class Date extends Pool1.Exercice5.Date {
    public int day;
    public int month;
    public int year;

    public Date() {
        this.day = 1;
        this.month = 1;
        this.year = 1;
    }

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    private Date(Date date) {
        if (date.day >= nbJours(date.month)) {
            if (date.month >= 12) {
                date.day = 1;
                date.month = 1;
                date.year++;
            } else {
                date.day = 1;
                date.month++;
            }
        } else {
            date.day++;
        }
    }

    public void lendemain(Date date) {
        new Date(date);
    }

    public String toString() {
        return this.day + "/" + this.month + "/" + this.year;
    }

    public int toInt() {
        return this.day + this.month + this.year;
    }
}
