package Pool1.Exercice1;

class Compteur {
    private static int cpt = 0;

    void plusUn() {
        cpt++;
    }

    int lecture() {
        return cpt;
    }
}
