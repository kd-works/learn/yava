package Pool1.Exercice13;

import Pool1.Exercice6.Date;

import java.util.Arrays;
import java.util.Random;

public class Memo extends Pool1.Exercice7.Memo {
    public static void main(String[] args) {
        melange();
    }

    private static void melange() {
        String[] old_tab = {new Date().toString()};
        String[] new_tab = {};

        Random r = new Random();

        for (int i = 0; i < old_tab.length; i++) {
            String shuffle = old_tab[r.nextInt(old_tab.length)];

            System.out.println(shuffle);

            new_tab[i] = shuffle;
        }

        System.out.println(Arrays.toString(old_tab));
        System.out.println(Arrays.toString(new_tab));
    }
}
