package Pool1.Exercice4;

public class Date extends Pool1.Exercice3.Date {
    private int[] months = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    protected int nbJours(int month) {
        return this.months[month - 1];
    }
}
