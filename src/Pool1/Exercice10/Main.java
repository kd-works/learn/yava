package Pool1.Exercice10;

import Pool1.Exercice6.Date;

public class Main {
    public static void main(String[] args) {
        Periode periode = new Periode();

        Date start = new Date();
        Date end = new Date(2,1,1);

        boolean bool = periode.dateEndAfterStart(start, end);

        System.out.println(bool);
    }
}
