package Pool1.Exercice10;

import Pool1.Exercice6.Date;

class Periode extends Pool1.Exercice9.Periode {
    boolean dateEndAfterStart(Date start, Date end) {
        return end.toInt() >= start.toInt();
    }
}
