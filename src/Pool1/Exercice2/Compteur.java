package Pool1.Exercice2;

class Compteur {
    private int cpt;

    Compteur() {
        this.cpt = 0;
    }

    void plusUn() {
        this.cpt++;
    }

    int lecture() {
        return this.cpt;
    }
}
